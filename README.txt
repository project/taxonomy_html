Intorduction
------------------------------------------------------------------------------

This module provides an overview page and block representations of your
site's taxonomy. This is useful for enabling users to browse your site
according to topics of their own interest.

How to install?
------------------------------------------------------------------------------

This module requires and build upon the taxonomy module.

1. Copy this module to the Drupal modules/ directory. Drupal should
   automatically detect it. Then enable the module at administer >> modules.

2. Create lots of vocabularies and terms, if you haven't already.

3. The administer >> settings >> taxonomy_html page allows you to set
   vocabularies to omit from the display, the page title and the number
   of columns used to display the categories on the overview page.

3. Configure the taxonomy_html blocks you wish to display on the 
   administer >> blocks page.

4. Consider enabling the link to the overview page using 
   admininter >> menus.

5. Visit the taxonomy_html page to see the overview.

Author
------------------------------------------------------------------------------

Moshe Weitzman <weitzman@tejasa.com> is the original author of this module.
Gabor Hojtsy updated and modernized it for Drupal 4.7.
There is no current maintainer.